#!/bin/bash

export NODE_ENV=production
export env_config=prod

# git reset --hard HEAD
# git pull origin master

rm -rf dist/ node_modules/

npm install

npm run build:prod
