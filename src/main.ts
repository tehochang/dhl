import '@/styles/main.scss';

import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import vuetifyOptions from '@/plugins/vuetify/index';
import DatetimePicker from 'vuetify-datetime-picker';
import VueTelInput from 'vue-tel-input';
import browserDetect from 'vue-browser-detect-plugin';
import moment from 'moment';

import { createRouter } from '@/router';
import { createStore } from '@/store';
import { createTranslation, locale } from '@/translations';
import App from '@/App.vue';

import { UserAPI } from '@/api/user.api';
import { Helper } from '@/services/helper/helper.service';
import { Logger } from '@/services/logger/logger.service';

Vue.config.productionTip = false;

export const store = createStore(Vue);
const i18n = createTranslation(Vue);
const companyCode = process.env.VUE_APP_IS_PRIVATE ? process.env.VUE_APP_COMPANY_CODE.toLowerCase() : null;
const router = createRouter(Vue, store, companyCode);

Vue.use(Vuetify);
Vue.use(DatetimePicker);
Vue.use(VueTelInput);
Vue.use(browserDetect);

declare module 'vue/types/vue' {
  interface Vue {
    $addSnackbar: Function;
    $removeSnackbar: Function;
  }
}

Vue.mixin({
  methods : {
    $addSnackbar(payload) {
      if (payload && payload.type && payload.message) {
        this.$store.commit( 'common/ADD_SNACKBAR', {
          isShow: true,
          type: payload.type || 'success',
          message: payload.message || '',
        });
      }
    },
    $removeSnackbar(index) {
      this.$store.commit('common/REMOVE_SNACKBAR', index);
    }
  }
});

const vuetify =  new Vuetify({
  ...vuetifyOptions,
  lang: {
    t: (key, ...params) => <string> i18n.t(key, params),
    current: locale === 'en' ? 'en' : locale === 'sc' ? 'zh-cn' : locale === 'tc' ? 'zh-hk' : 'en',
  }
});

Vue.filter('convertTimezone', (d, timezone: number = 8, format: string = 'YY-MM-DD HH:mm') => {
  if (d === null || d === undefined) {
    return d;
  }

  d = d.toString();

  if (timezone !== 8) {
    const date = moment.utc(d).subtract(8, 'hours').utcOffset(timezone);
    return date.format(format);
  } else {
    return moment(d).format(format);
  }
});

const initVueApp = () => {
  new Vue({
    router,
    store,
    i18n,
    vuetify,
    render: (h) => h(App)
  }).$mount('#app');
};

const helper: Helper = new Helper();
const logger: Logger = new Logger();
const authToken: string = helper.getCookie(process.env.VUE_APP_AUTH_COOKIE);
const token: string = authToken ? authToken.split('|')[0] || null : null;

if (authToken) {
  const userRequest = new UserAPI(token);
  userRequest.myuser()
    .then((requestData) => {
      if (requestData) {
        store.dispatch('authentication/saveUserKey', authToken);
        store.dispatch('authentication/saveUserInformation', requestData);
      } else {
        store.dispatch('authentication/logOutUser');
      }

      initVueApp();
    })
    .catch((error) => {
      logger.error(error);
    });
} else {
  initVueApp();
}
