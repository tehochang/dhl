import { Mutation } from 'vuex-class';
import { Component, Vue } from 'vue-property-decorator';

@Component
export class ThemeMixin extends Vue {
  @Mutation('SET_DARK_THEME', { namespace: 'common' }) $setDarkTheme;

  get $isDarkTheme(): boolean {
    return this.$store.getters['common/isDarkTheme'];
  }

  set $isDarkTheme(newVal) {
    this.$setDarkTheme(newVal);
  }

  get getEchartTheme(): string {
    return this.$isDarkTheme ? 'lf-dark' : 'lf-light';
  }

  get isBtnTextDark() {
    let bool = false;

    if (
      process.env.VUE_APP_IS_PRIVATE &&
      (process.env.VUE_APP_COMPANY_CODE === 'CBIP')
    ) {
      bool = true;
    }

    return bool;
  }
}
