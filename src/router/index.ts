import Vue from 'vue';
import Router from 'vue-router';

import { Helper } from '@/services/helper/helper.service';

const helper: Helper = new Helper();

export const constantRoutes = (companyCode) => [
  {
    path: '/',
    name: 'login',
    component: () => {
      return import(/* webpackChunkName: "login" */ `@/views/login/templates/${companyCode || 'default'}.vue`);
    },
    meta: {
      isAuthenticated: false,
      layout: 'none',
    },
  },
  {
    path: '/page-not-found',
    name: 'notFound',
    component: () => import(/* webpackChunkName: "not-found" */ '@/views/not-found/not-found.vue'),
    meta: {
      isForEveryone: true,
      layout: 'none',
    },
  },
  {
    path: '/page-not-supported',
    name: 'notSupported',
    component: () => import(/* webpackChunkName: "not-supported" */ '@/views/not-supported/not-supported.vue'),
    meta: {
      isForEveryone: true,
      layout: 'none',
    },
  },
  {
    path: '/unauthorized',
    name: 'unauthorized',
    component: () => import(/* webpackChunkName: "unauthorized" */ '@/views/unauthorized/unauthorized.vue'),
    meta: {
      isForEveryone: true,
      layout: 'none',
    },
  },
];

export const companyRoutes = [
  {
    path: '/receipt/find',
    name: 'findReceipt',
    component: () => import(/* webpackChunkName: "receipt-find" */ '@/views/receipt-find/receipt-find.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/receipt/',
    name: 'newReceipt',
    component: () => import(/* webpackChunkName: "receipt-new" */ '@/views/receipt-new/receipt-new.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/list/picking/',
    name: 'pickingList',
    component: () => import(/* webpackChunkName: "list-picking" */ '@/views/list-picking/list-picking.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/list/upload/',
    name: 'uploadList',
    component: () => import(/* webpackChunkName: "list-upload" */ '@/views/list-upload/list-upload.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/inventory/view',
    name: 'viewInventory',
    component: () => import(/* webpackChunkName: "inventory-view" */ '@/views/inventory-view/inventory-view.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/inventory/adjustment/',
    name: 'adjustmentInventory',
    component: () => import(/* webpackChunkName: "inventory-adjustment" */ '@/views/inventory-adjustment/inventory-adjustment.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/report/receipt-summary/',
    name: 'receiptSummaryReport',
    component: () => import(/* webpackChunkName: "report-receipt-summary" */ '@/views/report-receipt-summary/report-receipt-summary.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/report/picking-summary/',
    name: 'pickingSummaryReport',
    component: () => import(/* webpackChunkName: "report-picking-summary" */ '@/views/report-picking-summary/report-picking-summary.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
  {
    path: '/report/inventory/',
    name: 'inventoryReport',
    component: () => import(/* webpackChunkName: "report-inventory" */ '@/views/report-inventory/report-inventory.vue'),
    meta: {
      isAuthenticated: true,
      layout: 'main',
    },
  },
];

export const routes = (companyCode) => [
  ...constantRoutes(companyCode),
  ...companyRoutes,
  {
    path: '*',
    redirect: '/page-not-found',
    meta: {
      isForEveryone: true,
      layout: 'none',
    },
  },
];

const handleRouterPermission = ({ router, store }) => {
  router.beforeEach((to, from, next) => {
    const $isLogIn = store.getters['authentication/isLogIn'] || false;
    const $userInformation = store.getters['authentication/userInformation'] || {};
    const $userKey = store.getters['authentication/userKey'] || {};

    if (to.matched.some((record) => record.meta.isForEveryone)) {
      next();
    } else {
      if (to.matched.some((record) => record.meta.isAuthenticated)) {
        if (!$isLogIn) {
          next({ name: 'login'});
        } else if ($userKey) {
          next();
        } else {
          next({ name: 'unauthorized' });
        }
      } else {
        if ($isLogIn) {
          const redirectRote: object = helper.getDefaultRouteAfterLogIn();
          next(redirectRote);
        } else {
          next();
        }
      }
    }
  });
};

export function createRouter(vueInstance = Vue, store, companyCode = '') {
  vueInstance.use(Router);

  const router = new Router({ routes: routes(companyCode) });
  handleRouterPermission({ router, store });

  return router;
}
