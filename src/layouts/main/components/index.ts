export { default as Navbar } from './navbar/navbar.vue';
export { default as Sidebar } from './sidebar/sidebar.vue';
export { default as Footer } from './footer/footer.vue';
