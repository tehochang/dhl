import { Component, Prop, Mixins, Watch } from 'vue-property-decorator';
import { Getter } from 'vuex-class';

import { HelperMixin } from '@/mixins/helper/helper';

import { userRoutes } from './sidebar.constants';

@Component({
  name: 'SidebarComponent',
  components: {},
})

export default class SidebarComponent extends Mixins(HelperMixin) {
  @Getter('isLogIn', { namespace: 'authentication' }) $isLogIn;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;

  @Prop({ type: Boolean, default: false }) isShowSidebar: boolean;

  private appName: string = process.env.VUE_APP_NAME;
  private appLogo: string = process.env.VUE_APP_LOGO;

  private isDisplaySidebar: boolean = false;

  private userRoutes: object[] = [ ...userRoutes ];

  @Watch('$route.path')
  onPathChanges() {
    this.$emit('onUpdateSidebar', false);
  }

  @Watch('isShowSidebar', { immediate: true })
  onShowSidebarChanges(newVal) {
    this.isDisplaySidebar = newVal;
  }


  get routeList() {
    return this.userRoutes;
  }

  created() {
    this.isDisplaySidebar = this.isShowSidebar;
  }

  showChildren(meta: any): void {
    meta.isHideChildren = !meta.isHideChildren;
  }

  getParentRouteClass(route) {
    let routeName = route.name;

    if (this.hasChildren(route) && route.children.length === 1) {
      routeName = route.children[0].name;
    }

    return {
      active: this.$route.name === routeName,
    };
  }

  getChildRouteClass(childRoute) {
    return {
      active: this.$route.name === childRoute.name,
    };
  }

  hasChildren(route) {
    return route.hasOwnProperty('children') && route.children.length > 0;
  }

  hasOneChildren(route) {
    return this.hasChildren(route) && route.children.length === 1;
  }

  hasMoreThanOneChildren(route) {
    return this.hasChildren(route) && route.children.length > 1;
  }
}
