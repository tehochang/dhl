export const userRoutes: object[] = [
  {
    name: 'receiving',
    meta: {
      isHideChildren: false,
    },
    children: [
      { name: 'findReceipt', path: '/receipt/find', },
      { name: 'newReceipt', path: '/receipt/', },
    ],
  },
  {
    name: 'picking',
    meta: {
      isHideChildren: false,
    },
    children: [
      { name: 'pickingList', path: '/list/picking', },
      { name: 'uploadList', path: '/list/upload', },
    ],
  },
  /*{
    name: 'inventory',
    meta: {
      isHideChildren: false,
    },
    children: [
      { name: 'viewInventory', path: '/inventory/view', },
      { name: 'adjustmentInventory', path: '/inventory/adjustment', },
    ],
  },*/
  {
    name: 'reporting',
    meta: {
      isHideChildren: false,
    },
    children: [
      { name: 'receiptSummaryReport', path: '/report/receipt-summary', },
      { name: 'pickingSummaryReport', path: '/report/picking-summary', },
      { name: 'inventoryReport', path: '/report/inventory', },
    ],
  },
];
