import { Getter, Action } from 'vuex-class';
import { Component, Mixins, Prop, Watch } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoggerMixin } from '@/mixins/logger/logger';
import { ThemeMixin } from '@/mixins/theme/theme';

import LanguageSelector from '@/components/language-selector/language-selector.vue';

import { userRoutes } from '../sidebar/sidebar.constants';

import { UserAPI } from '@/api/user.api';

@Component({
  name: 'NavbarComponent',
  components: {
    'language-selector': LanguageSelector,
  },
})

export default class NavbarComponent extends Mixins(HelperMixin, LoggerMixin, ThemeMixin) {
  @Getter('isLogIn', { namespace: 'authentication' }) $isLogIn;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;
  @Action('logOutUser', { namespace: 'authentication' }) $logOutUser;

  @Prop({ type: Boolean, default: false }) isShowSidebar: boolean;

  private userAPI: UserAPI = null;

  private appName: string = process.env.VUE_APP_NAME;

  private userRoutes: object[] = [ ...userRoutes ];

  private notificationData: Array<any> = [];
  private notificationCount: number = 0;

  private isMobile: boolean = false;

  private get appLogo(): string {
      return process.env.VUE_APP_LOGO;
  }

  private get homeRoute(): object {
    return this.helper.getDefaultRouteAfterLogIn();
  }

  private get notificationCountText(): string {
    if (this.notificationCount >= 10) {
      return '9+';
    } else {
      return this.notificationCount.toString();
    }
  }

  private get routeList() {
    return this.userRoutes;
  }

  created(): void {
    this.isMobile = this.helper.isMobile();

    const authToken: string = this.helper.getCookie(process.env.VUE_APP_AUTH_COOKIE);
    if (!this.userAPI && authToken) {
      this.userAPI = new UserAPI(authToken);
    }

    if (window.localStorage.getItem('isLoggedIn') && (!window.localStorage.getItem('hasNoAccess'))) {
      window.localStorage.removeItem('isLoggedIn');
    }
  }

  mounted() {
  }

  private updateSidebar(value: boolean = true): void {
    this.$emit('onUpdateSidebar', value);
  }

  private handleThemeChanged() {
    this.$isDarkTheme = !this.$isDarkTheme;
  }

  private logOutUser(): void {
    this.$logOutUser({ routePath: this.$route.fullPath, isReload: true });
  }

  hasChildren(route) {
    return route.hasOwnProperty('children') && route.children.length > 0;
  }

  hasOneChildren(route) {
    return this.hasChildren(route) && route.children.length === 1;
  }

  hasMoreThanOneChildren(route) {
    return this.hasChildren(route) && route.children.length > 1;
  }

  getParentRouteClass(route) {
    let routeName = route.name;

    if (this.hasChildren(route) && route.children.length === 1) {
      routeName = route.children[0].name;
    }

    return {
      active: this.$route.name === routeName,
    };
  }

  getChildRouteClass(childRoute) {
    return {
      active: this.$route.name === childRoute.name,
      highlighted: childRoute.name === 'createOrder'
    };
  }
}
