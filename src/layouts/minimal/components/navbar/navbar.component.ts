import { Getter, Action } from 'vuex-class';
import { Component, Mixins, Prop } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoggerMixin } from '@/mixins/logger/logger';
import { ThemeMixin } from '@/mixins/theme/theme';

import moment from 'moment';

@Component({
  name: 'NavbarComponent',
  components: {},
})

export default class NavbarComponent extends Mixins(HelperMixin, LoggerMixin, ThemeMixin) {
  @Getter('isLogIn', { namespace: 'authentication' }) $isLogIn;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;
  @Action('changeLanguage', { namespace: 'common' }) $changeLanguage;
  @Action('logOutUser', { namespace: 'authentication' }) $logOutUser;

  private appName: string = process.env.VUE_APP_NAME;

  private languages: object[] = [];

  private get appLogo(): string {
    return process.env.VUE_APP_LOGO;
}

  private get homeRoute(): object {
    return this.helper.getDefaultRouteAfterLogIn();
  }

  created(): void {
    if (window.localStorage.getItem('isLoggedIn') && (!window.localStorage.getItem('hasNoAccess'))) {
      window.localStorage.removeItem('isLoggedIn');
    }

    this.languages = [
      { code: 'en', label: this.$t('language.english') },
      { code: 'sc', label: this.$t('language.simplifiedChinese') },
      { code: 'tc', label: this.$t('language.traditionalChinese') },
    ];
  }

  private setLang(langCode: string) {
    if (langCode !== this.$i18n.locale) {
      const messages = this.$i18n.getLocaleMessage(langCode);
      if (Object.keys(messages).length === 0) {
        this.$i18n.setLocaleMessage(langCode, require(`../../../../translations/${langCode}.json`));
      }

      this.$i18n.locale = langCode;
      moment.locale(langCode);
      this.$vuetify.lang.current = this.getVuetifyLangCode(langCode);
      this.$changeLanguage(langCode);
    }
  }

  private getVuetifyLangCode(langCode): string {
    switch (langCode) {
      case 'en':
        return 'en';
      case 'sc':
        return 'zh-cn';
      case 'tc':
        return 'zh-hk';
      default:
        return 'en';
    }
  }

  private handleThemeChanged() {
    this.$isDarkTheme = !this.$isDarkTheme;
  }

  private logOutUser(): void {
    this.$logOutUser({ routePath: this.$route.fullPath, isReload: true });
  }

  hasChildren(route) {
    return route.hasOwnProperty('children') && route.children.length > 0;
  }

  hasOneChildren(route) {
    return this.hasChildren(route) && route.children.length === 1;
  }

  hasMoreThanOneChildren(route) {
    return this.hasChildren(route) && route.children.length > 1;
  }

  getParentRouteClass(route) {
    let routeName = route.name;

    if (this.hasChildren(route) && route.children.length === 1) {
      routeName = route.children[0].name;
    }

    return {
      active: this.$route.name === routeName,
    };
  }

  getChildRouteClass(childRoute) {
    return {
      active: this.$route.name === childRoute.name,
      highlighted: childRoute.name === 'createOrder'
    };
  }

  goTo(routeName: string) {
    this.$router.push({ name: routeName });
  }
}
