export interface ScssVariables {
  blacker: string;
  black: string;
  white: string;
  red: string;
  yellow: string;
  green: string;
  'bay-blue': string;
  'bay-blue-dark': string;
  'bay-blue-darker': string;
  'table-border-color-light': string;
  'sm-min': number;

  'gogo-blue': string;
  'sf-red': string;
}

export const SCSS_VARIABLES: ScssVariables;

export default SCSS_VARIABLES;
