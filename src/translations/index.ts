import Vue from 'vue';
import VueI18n from 'vue-i18n';
import {
  en as enLocale,
  zhHans as scLocale,
  zhHant as tcLocale,
} from 'vuetify/src/locale';

import moment from 'moment';

import { Helper } from '@/services/helper/helper.service';

const helper: Helper = new Helper();
export let locale = helper.getCookie(process.env.VUE_APP_LOCALE_COOKIE);

export function createTranslation(vueInstance = Vue) {
  vueInstance.use(VueI18n);

  if (!locale) {
    locale = process.env.VUE_APP_LOCALE || 'en';
    helper.setCookie(process.env.VUE_APP_LOCALE_COOKIE, locale, helper.getDomain(), 90);
  }

  const messages: any = {
    en: {
      ...require('./en.json'),
      $vuetify: enLocale,
    },
    sc: {
      ...require('./sc.json'),
      $vuetify: scLocale,
    },
    tc: {
      ...require('./tc.json'),
      $vuetify: tcLocale,
    }
  };

  // moment.updateLocale('en', messages.en.moment);
  moment.defineLocale('sc', messages.sc.moment);
  moment.defineLocale('tc', messages.tc.moment);

  moment.locale(locale);

  return new VueI18n({
    locale: locale,
    fallbackLocale: 'en',
    silentTranslationWarn: true,
    messages,
  });
}
