import { Action, Getter, Mutation } from 'vuex-class';
import { Component, Watch, Prop, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { ThemeMixin } from '@/mixins/theme/theme';

import moment from 'moment';

@Component({
  name: 'LanguageSelector',
})

export default class LanguageSelector extends Mixins(HelperMixin, ThemeMixin, ) {
  @Action('changeLanguage', { namespace: 'common' }) $changeLanguage;

  @Prop({ type: Boolean, default: false }) isAbsolute: boolean;

  private isMobile: boolean = false;

  created() {
    this.isMobile = this.helper.isMobile();
  }

  private get languages() {
    return [
      { code: 'en', label: this.$t('language.english') },
      { code: 'sc', label: this.$t('language.simplifiedChinese') },
      { code: 'tc', label: this.$t('language.traditionalChinese') },
    ];
  }

  private get btnStyle() {
    if (!this.isAbsolute) {
      return {
        padding: this.isMobile ? '0 !important' : '0 16px',
      };
    }

    return {
      position: 'absolute',
      top: '0.5rem',
      right: '0.5rem',
    };
  }

  private setLang(langCode: string) {
    if (langCode !== this.$i18n.locale) {
      const messages = this.$i18n.getLocaleMessage(langCode);
      if (Object.keys(messages).length === 0) {
        this.$i18n.setLocaleMessage(langCode, require(`../../translations/${langCode}.json`));
      }

      this.$i18n.locale = langCode;
      moment.locale(langCode);
      this.$vuetify.lang.current = this.getVuetifyLangCode(langCode);
      this.$changeLanguage(langCode);
    }
  }

  private getVuetifyLangCode(langCode): string {
    switch (langCode) {
      case 'en':
        return 'en';
      case 'sc':
        return 'zh-cn';
      case 'tc':
        return 'zh-hk';
      default:
        return 'en';
    }
  }
}
