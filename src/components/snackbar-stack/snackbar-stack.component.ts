import { Getter, Mutation } from 'vuex-class';
import { Component, Watch, Prop, Mixins } from 'vue-property-decorator';
import { HelperMixin } from '@/mixins/helper/helper';

@Component({
  name: 'SnackbarStack',
})

export default class SnackbarStack extends Mixins(HelperMixin) {
  @Getter('snackbars', { namespace: 'common' }) $snackbars;
  @Mutation('ADD_SNACKBAR', { namespace: 'common' }) $addSnackbar;
  @Mutation('REMOVE_SNACKBAR', { namespace: 'common' }) $removeSnackbar;

  @Prop({ type: Number, default: 10000 }) timeoutDuration: number;

  @Watch('$snackbars')
  onSnackbarsChanged(newVal) {
    this.helper.forEach(newVal, (item, index) => {
      setTimeout(() => {
        this.$removeSnackbar(index);
      }, this.timeoutDuration);
    });
  }
}
