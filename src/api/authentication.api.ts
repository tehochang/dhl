import { LegacyRequest, ILegacyResponseProps } from '@/services/request/legacy-request.service';

export class AuthAPI {
  protected legacyRequest: LegacyRequest;

  constructor() {
    this.legacyRequest = new LegacyRequest();
  }

  login(params: object = {}, data: object = {}) {
    return this.legacyRequest.post('VUE_APP_API_LEGACY', `v1/auth/login?test=${process.env.VUE_APP_DEBUG_MODE || 'false'}`, params, data)
      .then((response: ILegacyResponseProps) => {
        if (response.status && response.status === 'success') {
          return response || null;
        } else {
          return null;
        }
      })
      .catch (
        (error: any) => null
      );
  }
}
