import { LegacyRequest, ILegacyResponseProps } from '@/services/request/legacy-request.service';

export class UserAPI {
  protected legacyRequest: LegacyRequest;

  constructor(tokenState: string = '') {
    this.legacyRequest = new LegacyRequest(tokenState);
  }

  myuser(params: object = {}) {
    return this.legacyRequest.get('VUE_APP_API_LEGACY', 'v1/core/myuser', params)
      .then((request: ILegacyResponseProps) => {
        if (request.status && request.status === 'success') {
          return request.data || null;
        } else {
          return null;
        }
      })
      .catch (
        (error: any) => null
      );
  }
}
