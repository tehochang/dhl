import { WMSRequest, } from '@/services/request/wms-request.service';

export class WarehouseAPI {
  protected wmsRequest: WMSRequest;

  constructor(tokenState: object = null, isFile: boolean = false) {
    this.wmsRequest = new WMSRequest(tokenState, isFile);
  }

  getPODetails({ poNumber, requestParameters }) {
    return this.wmsRequest.get('VUE_APP_API_WMS', `v1/po/${poNumber}`, requestParameters);
  }

  getReceiveHistory({ poNumber, requestParameters }) {
    return this.wmsRequest.get('VUE_APP_API_WMS', `v1/po/${poNumber}/receive-history`, requestParameters);
  }

  stockIn({ poNumber, payload }) {
    return this.wmsRequest.post('VUE_APP_API_WMS', `v1/po/${poNumber}/receive`, {}, payload);
  }

  filterMatchPickingFromExcel({ payload }) {
    return this.wmsRequest.post('VUE_APP_API_WMS', `v1/picking/match`, {}, payload);
  }

  getAllConstants({ type, requestParameters }) {
    const params = {
      limit: 100,
      offset: 0,
      active: 'y',
      ...requestParameters,
    };

    return this.wmsRequest.get('VUE_APP_API_WMS', `v1/${type}`, params);
  }

  getConstant({ type, id, requestParameters, }) {
    return this.wmsRequest.get('VUE_APP_API_WMS', `v1/${type}/${id}`, requestParameters);
  }

  insertConstant({ type, payload, }) {
    return this.wmsRequest.post('VUE_APP_API_WMS', `v1/${type}`, {}, payload);
  }

  updateConstant({ type, id, payload, }) {
    return this.wmsRequest.put('VUE_APP_API_WMS', `v1/${type}/${id}`, {}, payload);
  }

  deleteConstant({ type, id, }) {
    return this.wmsRequest.delete('VUE_APP_API_WMS', `v1/${type}/${id}`);
  }
}
