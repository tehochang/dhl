import { MutationTree } from 'vuex';
import { AuthenticationState } from './types';

export const mutations: MutationTree<AuthenticationState> = {
  SET_IS_SUPER_ADMIN: (state: AuthenticationState, data: boolean) => {
    state.isSuperAdmin = data;
  },
  SET_IS_ADMIN: (state: AuthenticationState, data: boolean) => {
    state.isAdmin = data;
  },
  SET_IS_LOGIN: (state: AuthenticationState, data: boolean) => {
    state.isLogIn = data;
  },
  SET_USER_KEY: (state: AuthenticationState, data: string) => {
    state.userKey = data;
  },
  SET_TOKEN: (state: AuthenticationState, data: string) => {
    state.token = data;
  },
  SET_CUST_KEY: (state: AuthenticationState, data: string) => {
    state.custKey = data;
  },
  SET_API_TOKEN: (state: AuthenticationState, data: string) => {
    state.apiToken = data;
  },
  SET_USER_INFORMATION: (state: AuthenticationState, data: object) => {
    state.userInformation = data;
  },
};
