import { ActionTree } from 'vuex';
import { AuthenticationState } from './types';
import { RootState } from '../types';

import { Helper } from '@/services/helper/helper.service';

import { AuthAPI } from '@/api/authentication.api';
import { UserAPI } from '@/api/user.api';

const helper: Helper = new Helper();
const authRequest: AuthAPI = new AuthAPI();

export const actions: ActionTree<AuthenticationState, RootState> = {
  logInUser: ({ commit }, { loginData, remember }): Promise<any> => {
    const params: object = {};
    if (loginData['companyId']) {
      params['companyId'] = loginData['companyId'] || null;
    }

    return authRequest.login(params, loginData)
      .then((data) => {
        if (!data) {
          return false;
        }

        const expiry: number = (remember) ? 10 : 1;

        const token: string = data['data']['token'];
        const custKey: string = data['data']['custKey'];
        const apiToken: string = data['data']['apiToken'];
        const userKey: string = `${token}|${custKey}|${apiToken}`;
        const userRequest = new UserAPI(token);

        commit('SET_USER_KEY', userKey);
        commit('SET_TOKEN', token);
        commit('SET_CUST_KEY', custKey);
        commit('SET_API_TOKEN', apiToken);

        return userRequest.myuser()
          .then((responseData) => {
            if (responseData) {
              const domain: string = helper.getDomain();
              helper.setCookie(process.env.VUE_APP_AUTH_COOKIE, userKey, domain, expiry);

              commit('SET_IS_LOGIN', true);
              commit('SET_USER_INFORMATION', responseData);

              return helper.getDefaultRouteAfterLogIn();
            } else {
              return false;
            }
          })
          .catch((error) => false);
      })
      .catch(() => false);
  },
  logOutUser: ({ commit, state }, payload): void => {
    const resetState = () => {
      commit('SET_IS_LOGIN', false);
      commit('SET_USER_KEY', '');
      commit('SET_TOKEN', '');
      commit('SET_CUST_KEY', '');
      commit('SET_API_TOKEN', '');
      commit('SET_USER_INFORMATION', {});
    };


    if (!payload) {
      resetState();
      window.localStorage.setItem('isLoggedOut', '1');
      return;
    }

    const { routePath, isReload } = payload;
    const domain = helper.getDomain();
    helper.deleteCookie(process.env.VUE_APP_AUTH_COOKIE, domain);

    resetState();

    window.localStorage.setItem('rQuery', helper.encode(routePath.substring(1)));
    window.localStorage.setItem('isLoggedOut', '1');

    if (isReload) {
      window.location.reload();
    }
  },
  saveUserKey: ({ commit }, data: string): void => {
    const token: string = data ? data.split('|')[0] || null : null;
    const custKey: string = data ? data.split('|')[1] || null : null;
    const apiToken: string = data ? data.split('|')[2] || null : null;

    commit('SET_USER_KEY', data);
    commit('SET_TOKEN', token);
    commit('SET_CUST_KEY', custKey);
    commit('SET_API_TOKEN', apiToken);
  },
  saveUserInformation: ({ commit }, userInformation: object): void => {
    commit('SET_IS_LOGIN', true);
    commit('SET_USER_INFORMATION', userInformation);
  }
};
