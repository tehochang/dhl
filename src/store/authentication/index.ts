import { Module } from 'vuex';
import { getters } from './getters';
import { actions } from './actions';
import { mutations } from './mutations';
import { AuthenticationState } from './types';
import { RootState } from '../types';
import { Helper } from '@/services/helper/helper.service';

const helper: Helper = new Helper();

export const state: AuthenticationState = {
  isSuperAdmin: false,
  isAdmin: false,
  isLogIn: false,
  userKey: '',
  token: '',
  custKey: '',
  apiToken: '',
  userInformation: {},
};

const namespaced: boolean = true;

const AuthenticationModule: Module<AuthenticationState, RootState> = {
  namespaced,
  state,
  getters,
  mutations,
  actions,
};

export function createAuthenticationModule() {
  return AuthenticationModule;
}
