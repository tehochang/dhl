export interface AuthenticationState {
  isSuperAdmin: boolean;
  isAdmin: boolean;
  isLogIn: boolean;
  userKey: string;
  token: string;
  custKey: string;
  apiToken: string;
  userInformation: object;
}
