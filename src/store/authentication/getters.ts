import { GetterTree } from 'vuex';
import { AuthenticationState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<AuthenticationState, RootState> = {
  isSuperAdmin(state: AuthenticationState): boolean {
    return state.isSuperAdmin;
  },
  isAdmin(state: AuthenticationState): boolean {
    return state.isAdmin;
  },
  isLogIn(state: AuthenticationState): boolean {
    return state.isLogIn;
  },
  userKey(state: AuthenticationState): string {
    return (state.isLogIn) ? state.userKey : '';
  },
  token(state: AuthenticationState): string {
    return (state.token) ? state.token : '';
  },
  custKey(state: AuthenticationState): string {
    return (state.custKey) ? state.custKey : '';
  },
  apiToken(state: AuthenticationState): string {
    return (state.apiToken) ? state.apiToken : '';
  },
  userInformation(state: AuthenticationState): object {
    return (state.isLogIn) ? state.userInformation : {};
  },
};
