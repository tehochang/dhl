import { MutationTree } from 'vuex';
import { CommonState } from './types';

export const mutations: MutationTree<CommonState> = {
  SET_LANGUAGE: (state: CommonState, data: string): void => {
    state.language = data;
  },
  SET_LOADING: (state: CommonState, data: boolean): void => {
    state.loading = data;
  },
  SET_DARK_THEME: (state: CommonState, data: boolean): void => {
    state.isDarkTheme = data;
  },
  SET_IS_FULLSCREEN: (state: CommonState, data: boolean): void => {
    state.isFullscreen = data;
  },
  ADD_SNACKBAR: (state: CommonState, data: object[]): void => {
    state.snackbars = [
      ...state.snackbars,
      data
    ];
  },
  REMOVE_SNACKBAR: (state: CommonState, index: number): void => {
    state.snackbars[index]['isShow'] = false;
  },
};
