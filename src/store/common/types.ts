export interface CommonState {
  language: string;
  loading: boolean;
  isDarkTheme: boolean;
  isFullscreen: boolean;
  snackbars: object[];
}
