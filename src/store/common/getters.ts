import { GetterTree } from 'vuex';
import { CommonState } from './types';
import { RootState } from '../types';

export const getters: GetterTree<CommonState, RootState> = {
  language(state: CommonState): string {
    return state.language;
  },
  loading(state: CommonState): boolean {
    return state.loading;
  },
  isDarkTheme(state: CommonState): boolean {
    return state.isDarkTheme;
  },
  isFullscreen(state: CommonState): boolean {
    return state.isFullscreen;
  },
  snackbars(state: CommonState): object[] {
    return state.snackbars;
  },
};
