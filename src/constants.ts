export const DATE_RANGE_OPTIONS = (i18n) => [
  {
    text: i18n.t('dateRange.today'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.yesterday'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.last7'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.last30'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.last60'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 60);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.last90'),
    onClick(picker) {
      const start = new Date();
      const end = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.last180'),
    onClick(picker) {
      const end = new Date();
      const start = new Date();
      start.setTime(start.getTime() - 3600 * 1000 * 24 * 180);
      end.setTime(end.getTime() - 3600 * 1000 * 24);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.thisMonth'),
    onClick(picker) {
      const today = new Date();
      const start = new Date(today.getFullYear(), today.getMonth(), 1);
      const end = new Date(today.getFullYear(), today.getMonth() + 1, 0);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  },
  {
    text: i18n.t('dateRange.lastMonth'),
    onClick(picker) {
      const today = new Date();
      const start = new Date(today.getFullYear(), today.getMonth() - 1, 1);
      const end = new Date(start.getFullYear(), start.getMonth() + 1, 0);
      start.setHours(0, 0, 0, 0);
      end.setHours(23, 59, 59, 59);
      picker.$emit('pick', [start, end]);
    }
  }
];
