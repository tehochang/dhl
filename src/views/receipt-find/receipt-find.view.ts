import { Getter } from 'vuex-class';
import { Component, Mixins, Watch } from 'vue-property-decorator';
import moment from 'moment';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

import { WarehouseAPI } from '@/api/wms.api';

@Component({
  name: 'ReceiptFind',
  components: {},
})

export default class ReceiptFind extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
  @Getter('token', { namespace: 'authentication' }) $token;
  @Getter('custKey', { namespace: 'authentication' }) $custKey;
  @Getter('apiToken', { namespace: 'authentication' }) $apiToken;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;

  private wmsAPI: WarehouseAPI = null;

  private fromDate: string = moment(new Date()).subtract(7, 'days').format('YYYY-MM-DD');
  private isShowFromDateMenu: boolean = false;
  private toDate: string = moment(new Date()).format('YYYY-MM-DD');
  private isShowToDateMenu: boolean = false;

  private poNum: string = '';
  private poNumVal: string = '';

  private data: any[] = [];
  private cartonHeaders: any[] = [];
  private cartonDetails: any[] = [];

  private isShowCartonHeaders: boolean = false;
  private isShowCartonDetails: boolean = false;

  private isMobile: boolean = false;

  private get itemTableHeaders(): object[] {
    return [
      { text: '#', value: 'index' },
      { text: this.$t('fields.receiveNo'), value: 'receiveNo' },
      { text: this.$t('fields.receiveDate'), value: 'receiveDate' },
      { text: this.$t('fields.receiveSeq'), value: 'receiveSeq' },
      { text: this.$t('fields.cartonHeaders'), value: 'view', sortable: false, width: 40 },
    ];
  }

  private get tableCartonHeaders(): object[] {
    return [
      { text: '#', value: 'index' },
      { text: this.$t('fields.cartonNum'), value: 'cartonNum' },
      { text: this.$t('fields.cartonSeqNum'), value: 'cartonSeqNum' },
      { text: this.$t('fields.status'), value: 'status' },
      { text: this.$t('fields.stockLocationId'), value: 'stockLocationId' },
      { text: this.$t('fields.damageDescription'), value: 'damageDescription' },
      { text: this.$t('fields.createAt'), value: 'createAt' },
      { text: this.$t('fields.createBy'), value: 'createBy' },
      { text: this.$t('fields.updatedAt'), value: 'updatedAt' },
      { text: this.$t('fields.updatedBy'), value: 'updatedBy' },
    ];
  }

  private get tableCartonDetails(): object[] {
    return [
      { text: '#', value: 'index' },
      { text: this.$t('fields.lineNum'), value: 'lineNum' },
      { text: this.$t('fields.qty'), value: 'qty' },
      { text: this.$t('fields.damageDescription'), value: 'damageDescription' },
      { text: this.$t('fields.createAt'), value: 'createAt' },
      { text: this.$t('fields.createBy'), value: 'createBy' },
    ];
  }

  async created() {
    this.isMobile = this.helper.isMobile();
    this.initAPI();
  }

  private initAPI() {
    const uid: number = this.$userInformation.user.id;
    const username: string = this.$userInformation.user.name;
    const compId: string = this.$userInformation.company.id;

    if (!this.wmsAPI) {
      this.wmsAPI = new WarehouseAPI({
        token: this.$token,
        custkey: this.$custKey,
        apitoken: this.$apiToken,
        uname: username,
        uid: 1178,
        compid: 123,
        // uid: 10007,
        // compid: 417,
      });
    }
  }

  private async getData() {
    try {
      const res = await this.wmsAPI.getReceiveHistory({
        requestParameters: null,
        poNumber: this.poNum,
      });

      if (!(res && res.data) || res.status === 'no content') {
        this.$addSnackbar({
          type: 'warning',
          message: this.$t('notification.data.notExists'),
        });

        this.resetData();
        return;
      }

      this.poNumVal = this.poNum;
      this.poNum = '';
      this.data = [ ...res.data ];
    } catch (e) {
      this.logger.error(e);
    }
  }

  private resetData(): void {
    this.data = [];
    this.cartonHeaders = [];
    this.cartonDetails = [];
  }

  private fromDateMenuAllowedDates(val) {
    return moment(val).isSameOrBefore(this.toDate, 'days');
  }

  private toDateMenuAllowedDates(val) {
    return moment(val).isSameOrAfter(this.fromDate, 'days');
  }

  private itemTableIndex(item): number {
    return this.helper.findIndex(this.data, i => i.id === item.id);
  }

  private showCartonHeaders(data) {
    this.cartonHeaders = [ ...data['cartonHeaders'] ];
    this.isShowCartonHeaders = true;
  }

  private hideCartonHeadersModal() {
    this.isShowCartonHeaders = false;
    this.cartonHeaders = [];
  }

  private cartonHeaderIndex(item) {
    return this.helper.findIndex(this.cartonHeaders, i => i.id === item.id);
  }

  private showCartonDetails(data) {
    this.cartonDetails = [ ...data['cartonDetails'] ];
    this.isShowCartonDetails = true;
  }

  private hideCartonDetailsModal() {
    this.isShowCartonDetails = false;
    this.cartonDetails = [];
  }

  private cartonDetailIndex(item) {
    return this.helper.findIndex(this.cartonDetails, i => i.id === item.id);
  }
}
