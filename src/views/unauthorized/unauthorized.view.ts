import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component({
  name: 'Unauthorized',
})

export default class Unauthorized extends Vue {}
