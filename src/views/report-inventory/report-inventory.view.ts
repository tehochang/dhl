import { Component, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

@Component({
  name: 'ReportInventory',
  components: {
  },
})

export default class ReportInventory extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
  created(): void {}
}
