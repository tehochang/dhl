import { Getter } from 'vuex-class';
import { Component, Mixins } from 'vue-property-decorator';
import FormData from 'form-data';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

import { WarehouseAPI } from '@/api/wms.api';

@Component({
  name: 'ListUpload',
  components: {
    'list-picking': () => import('@/views/list-picking/list-picking.vue')
  },
})

export default class ListUpload extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
  @Getter('token', { namespace: 'authentication' }) $token;
  @Getter('custKey', { namespace: 'authentication' }) $custKey;
  @Getter('apiToken', { namespace: 'authentication' }) $apiToken;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;

  private excelFile: any = null;
  private excelFileName: string = '';
  private machedPickingList: object = {};
  private wmsAPI: WarehouseAPI = null;

  created(): void {
    this.machedPickingList = {};
  }

  showUpload(): void {
    (this.$refs.fileUpload as any).click();
  }

  preloadExcelFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      this.excelFile = event.target.files[0];
      this.excelFileName = event.target.files[0].name;
    }
  }

  async uploadExcel(): Promise<void> {
    const uid: number = this.$userInformation.user.id;
    const username: string = this.$userInformation.user.name;
    const compId: string = this.$userInformation.company.id;

    if (!this.wmsAPI) {
      this.wmsAPI = new WarehouseAPI({
        token: this.$token,
        custkey: this.$custKey,
        apitoken: this.$apiToken,
        uname: username,
        // uid: 1178,
        // compid: 123,
        uid: 10007,
        compid: 417,
      }, true);
    }

    const formData = new FormData();
    formData.append('file', this.excelFile);

    try {
      const machingResult = await this.wmsAPI.filterMatchPickingFromExcel({
        payload: formData
      });
      if (machingResult.error_code !== 0 || machingResult.err_desc !== null) {
        throw new Error(machingResult.err_desc);
      }
      this.machedPickingList = machingResult.data;
    } catch (error) {
      console.error(error);
    }
  }

  async cancelPicking(): Promise<void> {}

  async savePicking(): Promise<void> {}
}
