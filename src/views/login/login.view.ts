import { Component, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoggerMixin } from '@/mixins/logger/logger';

import LanguageSelector from '@/components/language-selector/language-selector.vue';
import LoginForm from './components/login-form/login-form.vue';

@Component({
  name: 'Login',
  components: {
    'language-selector': LanguageSelector,
    'login-form': LoginForm,
  },
})

export default class Login extends Mixins(HelperMixin, LoggerMixin) {
  private get appLogo(): string {
    return require(`@/assets/images/worker.png`);
  }

  private get appName(): string {
    return process.env.VUE_APP_NAME || 'WMS';
  }

  created(): void {
    this.handleLocalStorage();
  }

  private handleLocalStorage() {
    if (window.localStorage.getItem('isLoggedOut')) {
      window.localStorage.removeItem('isLoggedIn');
      window.localStorage.removeItem('isLoggedOut');
    }
  }
}
