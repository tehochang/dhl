import { Action } from 'vuex-class';
import { Component, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoggerMixin } from '@/mixins/logger/logger';
import { ThemeMixin } from '@/mixins/theme/theme';

import { VForm } from '@/types';

@Component({
  name: 'LoginForm',
  components: {
  },
})

export default class LoginForm extends Mixins(HelperMixin, LoggerMixin, ThemeMixin, ) {
  @Action('logInUser', { namespace: 'authentication' }) $logInUser;

  private formData: Object = {
    username: '',
    password: '',
    rememberMe: true
  };
  private usernameRules: Function[] = [];
  private passwordRules: Function[] = [];
  private isShowPwd: boolean = false;

  private get appLogo(): string {
    return require(`@/assets/images/logo.png`);
  }

  private get appName(): string {
    return process.env.VUE_APP_NAME || 'WMS';
  }

  private get form(): VForm {
    return this.$refs['form'] as VForm;
  }

  private async submitLogin() {
    const isFormValid: boolean = this.form.validate();

    if (!isFormValid) {
      return;
    }

    const payload: Object = {
      username: (this.formData['username'] || '').trim(),
      password: this.formData['password'].trim(),
    };

    if (process.env.VUE_APP_IS_PRIVATE) {
      payload['companyId'] = process.env.VUE_APP_COMPANY_ID || '';
    }

    this.logger.info(`form submitted: ${JSON.stringify(payload)}`);

    const data = await this.$logInUser({
      loginData: payload,
      remember: this.formData['rememberMe'] || true
    });

    if (!data) {
      this.$addSnackbar({
        type: 'error',
        message: this.$t('notification.login.failed'),
      });

      this.formData['password'] = '';
      return;
    }

    this.$addSnackbar({
      type: 'success',
      message: this.$t('notification.login.success'),
    });

    this.$router.push(data);
  }
}
