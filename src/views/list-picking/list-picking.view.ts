import { Component, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

@Component({
  name: 'ListPicking',
  props: {
    machedPickingList: {
      type: Object,
      default: {
        pickListItems: []
      }
    }
  }
})

export default class ListPicking extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
}
