import { Getter } from 'vuex-class';
import { Component, Mixins, Watch } from 'vue-property-decorator';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

import { WarehouseAPI } from '@/api/wms.api';

@Component({
  name: 'ReceiptNew',
  components: {},
})

export default class ReceiptNew extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
  @Getter('token', { namespace: 'authentication' }) $token;
  @Getter('custKey', { namespace: 'authentication' }) $custKey;
  @Getter('apiToken', { namespace: 'authentication' }) $apiToken;
  @Getter('userInformation', { namespace: 'authentication' }) $userInformation;

  private wmsAPI: WarehouseAPI = null;

  private requiredRules: Function[] = [];

  private poNum: string = '';
  private poNumVal: string = '';

  private data: object = null;
  private tableData: object[] = [];

  private selectedItems: object[] = [];
  private isShowEditDialog: boolean = false;
  private isConsolidate: boolean = false;

  private cartonNum: number = 0;
  private cartonQty: number = 0;
  private cartonLength: number = 0;
  private cartonWidth: number = 0;
  private cartonHeight: number = 0;
  private cartonVolume: number = 0;

  private itemCartons: object[] = [];
  private bufferItemCarton: object = {};
  private consolidatedItems: object[] = [];

  private damages: object[] = [];
  private stockLocations: object[] = [];

  private warehouseLocation: any = null;
  private lastCartonSeqNum: number = 0;

  @Watch('selectedItems')
  onVisibleChanged(newVal) {
    const isConsolidate = (newVal && newVal.length >= 2);
    this.isConsolidate = isConsolidate;
    this.cartonNum = isConsolidate ? 1 : 0;
  }

  private get isEditItemCartonBtnDisabled() {
    if (!this.isConsolidate) {
      return (this.cartonNum === 0 || this.cartonQty === 0);
    }

    return false;
  }

  private get itemTableHeaders(): object[] {
    return [
      { text: '#', value: 'index' },
      { text: this.$t('fields.materialCode'), value: 'productCode' },
      { text: this.$t('fields.description'), value: 'productName' },
      { text: this.$t('fields.orderDept'), value: 'productName' },
      { text: this.$t('fields.unit'), value: 'uom' },
      { text: this.$t('fields.poQty'), value: 'poQty' },
      { text: this.$t('fields.received'), value: 'receivedQty' },
      { text: this.$t('fields.recCRT'), value: 'recCRT' },
    ];
  }

  private get selectedItemsTableHeaders(): object[] {
    return [
      { text: '#', value: 'index' },
      { text: this.$t('fields.materialCode'), value: 'productCode' },
      { text: this.$t('fields.description'), value: 'productName' },
      { text: this.$t('fields.unit'), value: 'uom' },
      { text: this.$t('fields.poQty'), value: 'poQty' },
    ];
  }

  private get itemCartonHeaders(): object[] {
    return [
      { text: this.$t('fields.cartonNum'), value: 'cartonNum', width: 150 },
      // { text: this.$t('fields.doNum'), value: 'doNum', width: 150 },
      { text: this.$t('fields.qty'), value: 'qty' },
      // { text: this.$t('fields.weight'), value: 'weight' },
      // { text: this.$t('fields.length'), value: 'length' },
      // { text: this.$t('fields.width'), value: 'width' },
      // { text: this.$t('fields.height'), value: 'height' },
      // { text: this.$t('fields.volume'), value: 'volume' },
      // { text: this.$t('fields.totalVolume'), value: 'totalVolume' },
      { text: this.$t('fields.remarks'), value: 'remarks', width: 650 },
      { text: this.$t('fields.damage'), value: 'damage' }
    ];
  }

  private get totalSelectedItemsQty(): number {
    if (this.selectedItems.length === 0) {
      return 0;
    }

    return this.helper.sumBy(this.selectedItems, 'poQty') || 0;
  }

  private get totalItemCartonQty(): number {
    if (this.itemCartons.length === 0) {
      return 0;
    }

    return this.helper.sumBy(this.itemCartons, i => Number(i['qty'])) || 0;
  }

  private get isItemCartonValid(): boolean {
    if (this.selectedItems.length === 0) {
      return false;
    }

    const num = Number(this.totalItemCartonQty) + Number(this.cartonNum) * Number(this.cartonQty);
    return num <= this.totalSelectedItemsQty;
  }

  private get deliveryDate() {
    if (!this.data) {
      return null;
    }

    return moment(this.data['expDivDate']).format('YYYY-MM-DD HH:mm:ss');
  }

  private get splitItemCartons() {
    const arr = [];

    this.helper.forEach(this.bufferItemCarton, (val, key) => {
      if (!val['isConsolidate']) {
        arr.push(val);
      }
    });

    return arr.map(i => i['materialCode']);
  }

  private get consolidateItemCartons() {
    const arr = [];

    this.helper.forEach(this.bufferItemCarton, (val, key) => {
      if (val['isConsolidate']) {
        arr.push(val);
      }
    });

    return arr;
  }

  async created() {
    this.requiredRules = [
      v => !!v || this.$t('placeholder.fieldIsRequired'),
    ];

    this.initAPI();
    await this.getConstants();
  }

  private initAPI() {
    const uid: number = this.$userInformation.user.id;
    const username: string = this.$userInformation.user.name;
    const compId: string = this.$userInformation.company.id;

    if (!this.wmsAPI) {
      this.wmsAPI = new WarehouseAPI({
        token: this.$token,
        custkey: this.$custKey,
        apitoken: this.$apiToken,
        uname: username,
        uid: 1178,
        compid: 123,
        // uid: 10007,
        // compid: 417,
      });
    }
  }

  private async getConstants() {
    return Promise.all([
      this.wmsAPI.getAllConstants({ type: 'damage', requestParameters: null, }),
      this.wmsAPI.getAllConstants({ type: 'stock-location', requestParameters: null, }),
    ]).then(responses => {
      if (this.helper.some(responses, res => res.status === 'failed')) {
        return false;
      }

      const damages = this.helper.get(responses, '0.data', []);
      if (damages.length > 0) {
        this.damages = [ ...damages.filter(i => i['description'] !== 'Damage 1') ];
      }


      this.stockLocations = this.helper.get(responses, '1.data', []);
      if (this.stockLocations.length) {
        this.warehouseLocation = this.stockLocations[0];
      }

      return true;
    }).catch(err => {
      this.logger.error(err);
      return false;
    });
  }

  private async getData() {
    try {
      const res = await this.wmsAPI.getPODetails({
        requestParameters: null,
        poNumber: this.poNum,
      });

      if (!(res && res.data) || res.status === 'no content') {
        this.$addSnackbar({
          type: 'warning',
          message: this.$t('notification.data.notExists'),
        });

        this.resetData();
        return;
      }

      this.poNumVal = this.poNum;
      this.poNum = '';
      this.data = { ...res.data };
      this.tableData = [ ...this.data['poLines'], ];
      this.lastCartonSeqNum = this.data['lastCartonSeqNum'];
      // this.overwriteMockData();
    } catch (e) {
      this.logger.error(e);
    }
  }

  private overwriteMockData() {
    this.tableData = [
      ...this.data['poLines'],
      {
        actRcvDate: '1995-05-06T04:52:44.000Z',
        batchNum: '',
        compId: 123,
        createAt: '2014-10-12T22:07:45.000Z',
        createBy: 0,
        createdAt: '2021-01-14T02:58:47.341Z',
        createdBy: 0,
        divDate: '2010-02-03T20:17:05.000Z',
        divQty: 3410608,
        id: '3e3f590e-5a55-11eb-bdad-42010aaa0035',
        lineNotes: 'Aut deleniti accusamus itaque ex voluptatem eum itaque. Voluptatum sit repellat et repudiandae occaecati. Rem hic ratione voluptas dolorem quia qui beatae.',
        lineNum: 0,
        lineStatus: 0,
        modifyAt: '1982-05-24T18:24:01.000Z',
        modifyBy: 0,
        poNum: '123',
        poQty: 10,
        productCode: 'abc',
        productName: 'harum',
        rcvDate: '1971-01-02T19:58:25.000Z',
        recId: 1,
        remainQty: 0,
        uom: '',
        updatedAt: '2021-01-19T04:54:49.224Z',
        updatedBy: 0,
      },
      {
        actRcvDate: '1995-05-06T04:52:44.000Z',
        batchNum: '',
        compId: 123,
        createAt: '2014-10-12T22:07:45.000Z',
        createBy: 0,
        createdAt: '2021-01-14T02:58:47.341Z',
        createdBy: 0,
        divDate: '2010-02-03T20:17:05.000Z',
        divQty: 3410608,
        id: '3e3f590e-5a55-11eb-bdad-42010aaa003x',
        lineNotes: 'Aut deleniti accusamus itaque ex voluptatem eum itaque. Voluptatum sit repellat et repudiandae occaecati. Rem hic ratione voluptas dolorem quia qui beatae.',
        lineNum: 0,
        lineStatus: 0,
        modifyAt: '1982-05-24T18:24:01.000Z',
        modifyBy: 0,
        poNum: '123',
        poQty: 10,
        productCode: 'def',
        productName: 'bau',
        rcvDate: '1971-01-02T19:58:25.000Z',
        recId: 1,
        remainQty: 0,
        uom: '',
        updatedAt: '2021-01-19T04:54:49.224Z',
        updatedBy: 0,
      },
    ];

    this.tableData[0]['poQty'] = 50;
  }

  private resetData(): void {
    this.poNum = '';
    this.poNumVal = '';
    this.data = null;
    this.tableData = [];
    this.itemCartons = [];
    this.bufferItemCarton = {};
    this.consolidatedItems = [];
  }

  private handleItemSelected({ item, value }): void {
    if (value) {
      this.selectedItems.push(item);
    } else {
      this.selectedItems = this.selectedItems.filter(i => i['id'] !== item['id']);
    }
  }

  private handleToggleSelectAll({ items, value }): void {
    if (value) {
      items.forEach((item) => {
        this.selectedItems.push(item);
      });
    } else {
      items.forEach((item) => {
        this.selectedItems = this.selectedItems.filter(i => i['id'] !== item['id']);
      });
    }
  }

  private itemTableIndex(item): number {
    return this.helper.findIndex(this.tableData, i => i.id === item.id);
  }

  /*private isItemAllowSelected(item) {
    return true;

    // if (!this.isItemEdited(item)) {
    //   return true;
    // }

    const id = item['id'];

    if (this.isItemEdited(item)) {
      const isConsolidate = this.bufferItemCarton[id]['isConsolidate'];

      if (!isConsolidate) {
        if (!this.isConsolidate) {
          console.log('a', this.selectedItems.length, this.selectedItems[0], item['id'])

          if (this.selectedItems.length === 1) {
            console.log('a.1', this.selectedItems[0]['id'] === item['id'])
          }

          return (
            this.selectedItems.length === 0 ||
            (this.selectedItems.length === 1 && this.selectedItems[0]['id'] === item['id'])
          );
        }

        console.log('b', item['id'])
        // if (


        return !this.isItemEdited(item);
      }

      console.log('c', item['id'])
      return true;
    }

    if (this.helper.isEmptyObject(this.bufferItemCarton)) {
      console.log('d', item['id']);
      return true;
    }

    return !this.isItemEdited(item);
  }*/

  private editData(): void {
    this.isShowEditDialog = true;

    const item = this.selectedItems[0];

    if (this.isItemEdited(item)) {
      const id = item['id'];
      const data = this.bufferItemCarton[id]['data'];

      this.cartonLength = this.bufferItemCarton[id].length || 0;
      this.cartonWidth = this.bufferItemCarton[id].width || 0;
      this.cartonHeight = this.bufferItemCarton[id].height || 0;
      this.cartonVolume = this.bufferItemCarton[id].volume || 0;

      this.itemCartons = [ ...data ];
    }
  }

  private async saveData() {
    if (this.helper.isEmptyObject(this.bufferItemCarton) || this.helper.isUndefined(this.warehouseLocation)) {
      return;
    }

    const cartons = [];

    this.helper.forEach(this.bufferItemCarton, (i, key) => {
      if (!i['isConsolidate']) {
        this.helper.forEach(i['data'], (j, key2) => {
          const carton = {
            cartonNum: j['cNum'],
            cartonSeqNum: j['cartonSeqNum'],
            stockLocationId: this.warehouseLocation ? this.warehouseLocation['id'] : null,

            cartonDetails: [{
              poLineId: j['poLineId'],

              // doNum: j['doNum'],
              qty: j['qty'],
              // grandWeight: j['weight'],
              // dimL: j['length'],
              // dimW: j['width'],
              // dimH: j['height'],
              // cbm: j['volume'],
              // ttlCbm: j['totalVolume'],

              isDamage: j['damage'] ? true : false,
              damageDescription: j['damage'] || '',
            }]
          };

          cartons.push(carton);
        });
      }
    });

    const filteredBufferItemCarton = this.helper.filter(this.bufferItemCarton, i => i['isConsolidate']);
    const consolidatedCartons = this.helper.groupBy(filteredBufferItemCarton, i => i['consolidateWith']['id']);

    this.helper.forEach(consolidatedCartons, i => {
      const carton = {
        cartonNum: i[0]['data'][0]['cNum'],
        cartonSeqNum: i[0]['data'][0]['cartonSeqNum'],
        stockLocationId: this.warehouseLocation ? this.warehouseLocation['id'] : null,
        cartonDetails: []
      };

      this.helper.forEach(i, (c, idx) => {
        carton['cartonDetails'].push({
          poLineId: c['data'][idx]['poLineId'],

          // doNum: c['data'][idx]['doNum'],
          qty: c['data'][idx]['qty'],
          // grandWeight: c['data'][idx]['weight'],
          // dimL: c['data'][idx]['length'],
          // dimW: c['data'][idx]['width'],
          // dimH: c['data'][idx]['height'],
          // cbm: c['data'][idx]['volume'],
          // ttlCbm: c['data'][idx]['totalVolume'],

          isDamage: c['data'][idx]['damage'] ? true : false,
          damageDescription: c['data'][idx]['damage'] || '',
        });
      });

      cartons.push(carton);
    });

    const res = await this.wmsAPI.stockIn({
      poNumber: this.poNumVal,
      payload: {
        cartons
      },
    });

    if (!(res && res.data)) {
      this.$addSnackbar({
        type: 'warning',
        message: this.$t('notification.data.saveFailed'),
      });
      this.resetData();

      return;
    }

    this.$addSnackbar({
      type: 'success',
      message: this.$t('notification.data.saveSuccessful'),
    });
    this.resetData();
  }

  private selectedItemsTableIndex(item): number {
    return this.helper.findIndex(this.selectedItems, i => i.id === item.id);
  }

  private hideEditDialog(): void {
    this.isShowEditDialog = false;
    this.resetModification();
  }

  private resetModification(): void {
    this.cartonNum = 0;
    this.cartonQty = 0;
    this.cartonLength = 0;
    this.cartonWidth = 0;
    this.cartonHeight = 0;
    this.cartonVolume = 0;

    this.itemCartons = [];
  }

  private confirmModification(): void {
    if (!this.isConsolidate) {
      const id = this.selectedItems[0]['id'];
      const materialCode = this.selectedItems[0]['productCode'];

      this.bufferItemCarton = {
        ...this.bufferItemCarton,
        [id]: {
          id,
          materialCode,
          isConsolidate: this.isConsolidate,
          consolidateWith: null,

          weight: 0,
          length: this.cartonLength,
          width: this.cartonWidth,
          height: this.cartonHeight,
          volume: this.cartonVolume,
          totalVolume: 0,

          data: [ ...this.itemCartons ],
        },
      };
    } else {
      this.selectedItems.forEach(i => {
        const id = i['id'];
        const materialCode = i['productCode'];

        this.bufferItemCarton = {
          ...this.bufferItemCarton,
          [id]: {
            id,
            materialCode,
            isConsolidate: this.isConsolidate,
            consolidateWith: {
              id: this.selectedItems.map(x => x['id']),
              materialCode: this.selectedItems.map(x => x['productCode']),
            },

            weight: 0,
            length: this.cartonLength,
            width: this.cartonWidth,
            height: this.cartonHeight,
            volume: this.cartonVolume,
            totalVolume: 0,

            data: [ ...this.itemCartons ],
          },
        };
      });
    }

    this.isShowEditDialog = false;
    this.resetModification();
    this.selectedItems = [];
  }

  private handleCartonNumChange() {
    if (!this.cartonNum || this.isConsolidate) {
      return;
    }

    let itemCartonsQty = 0;
    if (this.itemCartons.length) {
      itemCartonsQty = this.helper.sumBy(this.itemCartons, 'qty');
    }

    const qty = this.selectedItems[0]['poQty'] - itemCartonsQty;
    const cartonQty = qty / this.cartonNum;

    this.cartonQty = Math.ceil(cartonQty);
  }

  private editItemsCarton() {
    if (!this.isConsolidate) {
      this.splitCartons();
      return;
    }

    this.consolidateCartons();
  }

  private splitCartons() {
    if (this.cartonNum === 0 || this.cartonQty === 0) {
      return;
    }

    const index = this.itemCartons.length === 0 ? this.lastCartonSeqNum : this.lastCartonSeqNum + this.itemCartons.length;

    for (let i = 0; i < Number(this.cartonNum); i++) {
      const cNum = `C${index + i + 1}`;

      this.itemCartons.push({
        id: uuidv4(),
        poLineId: this.selectedItems[0]['id'],
        cNum,
        cartonNum: `${this.poNumVal}-C${index + i + 1}`,
        cartonSeqNum: index + i + 1,

        qty: Number(this.cartonQty),

        remarks: '',
        damage: null,
      });
    }

    this.lastCartonSeqNum += Number(this.cartonNum);
    this.cartonNum = 0;
    this.cartonQty = 0;
  }

  private consolidateCartons() {
    if (this.cartonNum === 0) {
      return;
    }

    const index = this.itemCartons.length === 0 ? this.lastCartonSeqNum : this.lastCartonSeqNum + this.itemCartons.length;

    this.selectedItems.forEach((i, idx) => {
      const cNum = `C${index + 1}`;

      this.itemCartons.push({
        id: uuidv4(),
        poLineId: i['id'],
        cNum,
        cartonNum: `${this.poNumVal}-${cNum}`,
        cartonSeqNum: index + 1,

        qty: i['poQty'],

        remarks: '',
        damage: null,
      });
    });

    this.lastCartonSeqNum += Number(this.selectedItems.length);
    this.cartonNum = 1;
    this.cartonQty = 0;
  }

  private isItemEdited(item) {
    if (this.helper.isEmptyObject(this.bufferItemCarton)) {
      return false;
    }

    const id = item['id'];
    return this.bufferItemCarton.hasOwnProperty(id);
  }

  private getItemPOQty(item) {
    if (!this.isItemEdited(item)) {
      return 0;
    }

    const id = item['id'];
    const data = this.bufferItemCarton[id]['data'];
    const isConsolidate = this.bufferItemCarton[id]['isConsolidate'];
    const qty = this.helper.sumBy(data, i => Number(i['qty']));

    return !isConsolidate ? qty : item.poQty;
  }

  private getItemCartonQty(item) {
    if (!this.isItemEdited(item)) {
      return 0;
    }

    const id = item['id'];
    const data = this.bufferItemCarton[id]['data'];
    return this.bufferItemCarton[id]['isConsolidate'] ? 1 : data.length;
  }

  private calculateVol() {
    const l = this.cartonLength || 0;
    const w = this.cartonWidth || 0;
    const h = this.cartonHeight || 0;
    this.cartonVolume = (l * w * h);
  }
}
