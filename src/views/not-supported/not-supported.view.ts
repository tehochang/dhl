import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component({
  name: 'NotSupported',
})

export default class NotSupported extends Vue {
  private supportedBrowsers: object[] = [{
    name: 'chrome',
    company: 'google',
    link: 'https://www.google.com/intl/en_hk/chrome/',
  }, {
    name: 'firefox',
    company: 'mozilla',
    link: 'https://www.mozilla.org/en-US/firefox/new/',
  }, {
    name: 'safari',
    company: 'apple',
    link: 'https://support.apple.com/downloads/safari',
  }];

  private browserLogo(browser): string {
    return require(`@/assets/images/${browser}.svg`);
  }
}
