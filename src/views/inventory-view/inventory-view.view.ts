import { Component, Mixins } from 'vue-property-decorator';

import { HelperMixin } from '@/mixins/helper/helper';
import { LoadingMixin } from '@/mixins/loading/loading';
import { LoggerMixin } from '@/mixins/logger/logger';

@Component({
  name: 'InventoryView',
  components: {
  },
})

export default class InventoryView extends Mixins(HelperMixin, LoadingMixin, LoggerMixin) {
  created(): void {}
}
