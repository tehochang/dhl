import { light, dark } from './themes';

const opts = {
  theme: {
    themes: { light, dark },
    dark: false,
  }
};

export default opts;
