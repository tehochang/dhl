import colors from 'vuetify/lib/util/colors';
import SCSS_VARIABLES from '@/styles/mixins/_variables.scss';

export const light = {
  primary: SCSS_VARIABLES['bay-blue'],
  secondary: '#2288A0',
  accent: '#82B1FF',
  info: '#FFC107',
  success: colors.green.base,
  warning: colors.amber.base,
  error: '#FF5252',
};

export const dark = {
  primary: '#4E007F',
  secondary: '#2288A0',
  accent: '#82B1FF',
  info: colors.blueGrey.base,
  success: colors.green.base,
  warning: colors.amber.base,
  error: '#FF5252',
};
