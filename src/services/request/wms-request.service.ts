import axios from 'axios';

import { Logger } from '@/services/logger/logger.service';
import { Helper } from '@/services/helper/helper.service';

const logger = new Logger();
const helper = new Helper();

const handleResponse: Function = (res: any, type?: string) => {
  const errRes = {
    status: 'failed',
    message: (res.data && res.data.message) ? res.data.message : 'server-error',
    data: (res.data && res.data.data) ? res.data.data : {},
  };

  switch (res.status) {
    case 200:
      if (!res.data) {
        logger.error('Response Error', type, res);
        return errRes;
      }

      return res.data;
    case 204:
      return {
        status: 'no content',
        message: 'data not exists',
        data: (res.data && res.data.data) ? res.data.data : {},
      };
    default:
      logger.error('Response Error', type, res);
      return errRes;
  }
};

const handleError: Function = (error: any, type?: string) => {
  logger.error('Catch Error', type, error);
  return {
    status: 'failed',
    message: error,
  };
};

export class WMSRequest {
  service: any;

  constructor(auth: object, isFile: boolean = false) {
    const token = auth['token'];
    const allowedAuthProps = ['custkey', 'apitoken', 'uid', 'uname', 'compid', ];
    const headers = Object.keys(auth)
      .filter(key => allowedAuthProps.includes(key))
      .reduce((obj, key) => {
        obj[key] = auth[key];
        return obj;
      }, {});

    const additionalHeaders: object = {
      'Content-Type': (isFile) ? 'multipart/form-data' : 'application/json',
      Authorization: `Bearer ${token}`,
      ...headers,
    };

    const service = axios.create({
      headers: additionalHeaders,
    });

    this.service = service;
  }

   handleRequest(
    method: string = 'GET',
    name: string = 'VUE_APP_API_WMS',
    path: string = '',
    params: object = {},
    data: any = null,
    isFullPath: boolean = false,
  ) {
    const url: string = process.env[name.toUpperCase()] || '';
    const fullPath: string = (isFullPath) ?
      path :
      (helper.isNotEmpty(path)) ?
        `${url}/${path}` :
        url;

    params = helper.removeNullObject(params);

    const reqConfig = {
      method,
      url: fullPath,
      params,
    };

    if (data) {
      reqConfig['data'] = data;
    }

    return this.service
      .request(reqConfig, { crossDomain : true })
      .then((response: any) => handleResponse(response, method))
      .catch((error: any) => handleError(error, method));
  }

  get(
    name: string = 'VUE_APP_API_WMS',
    path: string = '',
    params: object = {},
    isFullPath: boolean = false
  ) {

    return this.handleRequest('GET', name, path, params, null, isFullPath);
  }

  post(
    name: string = 'VUE_APP_API_WMS',
    path: string = '',
    params: object = {},
    data: any = {},
    isFullPath: boolean = false
  ) {
    return this.handleRequest('POST', name, path, params, data, isFullPath);
  }

  put(
    name: string = 'VUE_APP_API_WMS',
    path: string = '',
    params: object = {},
    data: any = {},
    isFullPath: boolean = false
  ) {
    return this.handleRequest('PUT', name, path, params, data, isFullPath);
  }

  delete(
    name: string = 'VUE_APP_API_WMS',
    path: string = '',
    params: object = {},
    data: any = {},
    isFullPath: boolean = false
  ) {
    return this.handleRequest('DELETE', name, path, params, data, isFullPath);
  }
}
