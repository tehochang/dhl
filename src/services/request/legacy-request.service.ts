import axios from 'axios';

import { Logger } from '@/services/logger/logger.service';
import { Helper } from '@/services/helper/helper.service';

const logger = new Logger();

const handleResponse: Function = (res: any) => {
  const status: boolean = res.status === 200 && (res.data && (res.data.status === 200 || res.data.status === 'success'));

  return status ?
    res.data :
    {
      status: 'failed',
      message: (res.data && res.data.message) ? res.data.message : 'server-error',
      data: (res.data && res.data.data) ? res.data.data : {},
    };
};

const handleError: Function = (error: any, type?: string) => {
  logger.error(type, error);

  return ({
    status: 'failed',
    message: error,
  });
};

export interface ILegacyResponseProps {
  status: string;
  message: string;
  data?: object | (any|null)[] | null;
  pagination?: object | (any|null)[] | null;
}

export class LegacyRequest {
  helper: Helper;
  service: any;

  constructor(auth?: string, key?: string, isFile: boolean = false) {
    this.helper = new Helper();

    const additionalHeaders: object = {
      'Content-Type': (isFile) ? 'multipart/form-data' : 'application/json',
      [process.env.VUE_APP_LEGACY_KEY]: auth ? auth : key
        ? key
        : process.env.VUE_APP_LEGACY_KEY_ENCODED_HASH,
    };

    const service = axios.create({
      headers: additionalHeaders,
    });

    this.service = service;
  }

  get(name: string = 'VUE_APP_API', path: string = '', params: object = {}, isFullPath: boolean = false) {
    const url: string = process.env[name.toUpperCase()] || '';
    const fullPath: string = (isFullPath) ? path : (this.helper.isNotEmpty(path)) ? `${url}/${path}` : url;

    params = this.helper.removeNullObject(params);
    return this.service.get(fullPath, {params}, {crossDomain : true})
      .then((response: any) => handleResponse(response))
      .catch((error: any) => handleError(error, 'GET'));
  }

  getAll(paths: string[] = [], params: (object|null)[] = []) {
    const promises: (Promise<any>|null)[] = [];

    paths.forEach((dataVal, dataIndx) => {
      const nParams: object = this.helper.removeNullObject(params[dataIndx] || {});
      promises.push(this.get('', dataVal || '', nParams, true));
    });

    return Promise.all(promises)
      .then((results: any) => results)
      .catch((error: any) => handleError(error, 'GET ALL'));
  }

  post(name: string = 'VUE_APP_API', path: string = '', params: object = {}, data: any = {}, isFullPath: boolean = false) {
    const url: string = process.env[name.toUpperCase()] || '';
    const fullPath: string = (isFullPath) ? path : (this.helper.isNotEmpty(path)) ? `${url}/${path}` : url;

    params = this.helper.removeNullObject(params);
    return this.service.request({
        method: 'POST',
        url: fullPath,
        params,
        data,
      }, { crossDomain : true })
      .then((response: any) => handleResponse(response))
      .catch((error: any) => handleError(error, 'POST'));
  }

  put(name: string = 'core', path: string = '', params: object = {}, data: any = {}, isFullPath: boolean = false) {
    const url: string = process.env[name.toUpperCase()] || '';
    const fullPath: string = (isFullPath) ? path : (this.helper.isNotEmpty(path)) ? `${url}/${path}` : url;

    params = this.helper.removeNullObject(params);
    return this.service.request({
        method: 'PUT',
        url: fullPath,
        params,
        data,
      })
      .then((response: any) => handleResponse(response))
      .catch((error: any) => handleError(error, 'PUT'));
  }

  delete(name: string = 'core', path: string = '', params: object = {}, data: any = {}, isFullPath: boolean = false) {
    const url: string = process.env[name.toUpperCase()] || '';
    const fullPath: string = (isFullPath) ? path : (this.helper.isNotEmpty(path)) ? `${url}/${path}` : url;

    params = this.helper.removeNullObject(params);
    return this.service
      .request({
        method: 'DELETE',
        url: fullPath,
        params,
        data,
      })
      .then((response: any) => handleResponse(response))
      .catch((error: any) => handleError(error, 'DELETE'));
  }
}
