import moment from 'moment';
import {
  get, filter, findIndex, cloneDeep, includes, difference, differenceBy,
  intersection, intersectionBy, map, uniq, forEach, find, extend, sortBy,
  every, some, sum, uniqBy, sumBy, isEmpty, groupBy, flatten, orderBy,
  flow, chain, isEqual, values
} from 'lodash';
import {
  toJson, toString, isNotEmpty, getCookie, setCookie, deleteCookie,
  cleanDataRemoveNull, isInArray, decode, encode
} from 'jnpl-helper';
import SCSS_VARIABLES from '@/styles/mixins/_variables.scss';

export class Helper {
  env: string = process.env.NODE_ENV;

  get(data: any, value: string = '', defaultValue: any = ''): any {
    return get(data, value, defaultValue);
  }

  filter(data: any, func: any): any {
    return filter(data, func);
  }

  findIndex(collection: any, func: any): any {
    return findIndex(collection, func);
  }

  cloneDeep(value: any): any {
    return cloneDeep(value);
  }

  includes(...value: any): any {
    return includes(...value);
  }

  difference(...value: any): any {
    return difference(...value);
  }

  differenceBy(...value: any): any {
    return differenceBy(...value);
  }

  intersection(...value: any): any {
    return intersection(...value);
  }

  intersectionBy(...value: any): any {
    return intersectionBy(...value);
  }

  map(...value: any): any {
    return map(...value);
  }

  uniq(...value: any): any {
    return uniq(...value);
  }

  forEach(...value: any): any {
    return forEach(...value);
  }

  find(...value: any): any {
    return find(...value);
  }

  extend(...value: any): any {
    return extend(...value);
  }

  sortBy(...value: any): any {
    return sortBy(...value);
  }

  every(...value: any): any {
    return every(...value);
  }

  some(...value: any): any {
    return some(...value);
  }

  sum(value: any[]): any {
    return sum(value);
  }

  uniqBy(...value: any): any {
    return uniqBy(...value);
  }

  sumBy(...value: any): any {
    return sumBy(...value);
  }

  remove(...value: any): any {
    return sumBy(...value);
  }

  isEmpty(v: any = null): boolean {
    return isEmpty((v) ? v.toString() : null);
  }

  groupBy(...value: any): any {
    return groupBy(...value);
  }

  flatten(value: any): any {
    return flatten(value);
  }

  orderBy(...value: any): any {
    return orderBy(...value);
  }

  flow(...value: any): any {
    return flow(...value);
  }

  chain(value: any) {
    return chain(value);
  }

  isEqual(value: any, other: any) {
    return isEqual(value, other);
  }

  values(value: object) {
    return values(value);
  }

  toJson(jsonData: any = ''): any {
    return toJson(jsonData);
  }

  toString(jsonData: any = ''): any {
    return toString(jsonData);
  }

  isNotEmpty(v: any = null): boolean {
    return isNotEmpty((v) ? v.toString() : null);
  }

  isUndefined(v: any = null) {
    v = (v) ? (typeof(v) === 'string') ? v.toLowerCase() : v : null;

    return v == null || v === 'n/a' || v === '' || v === 'no' || v === 0 || v === 'undefined' || v === 'nan' || v === 'na' || v === ' ' || v === 'false' || v === 'null';
  }

  isInArray(value: any, array: any[]): boolean {
    return isInArray(value, array);
  }

  isEmptyObject(obj: Object = {}): boolean {
    return (Object.keys(obj).length === 0 && obj.constructor === Object);
  }

  getDomain() {
    return (process.env.NODE_ENV === 'development') ? '' : process.env.VUE_APP_DOMAIN;
  }

  setCookie(name: string = '', value: any = '', domain: string = '', exdays: number = 5): boolean {
    if (this.isUndefined(domain)) {
      domain = this.getDomain();
    }

    return setCookie(name, value, domain, exdays);
  }

  getCookie(name: string = ''): string {
    return getCookie(name);
  }

  deleteCookie(name: string = '', domain: string = ''): string {
    return deleteCookie(name, domain);
  }

  cleanDataRemoveNull(data: any): any {
    return cleanDataRemoveNull(data);
  }

  encode(data): string {
    return Buffer.from(data).toString('base64');
  }

  decode(data): string {
    return Buffer.from(data, 'base64').toString('ascii');
  }

  // add zero on the start of one digit numbers
  pad(number: number): string {
    return (number < 10) ? '0' + number : number.toString();
  }

  toggleTheme(element, className) {
    if (!element || !className) {
      return;
    }

    let classString = element.className;
    const nameIndex = classString.indexOf(className);

    if (classString !== '') {
      const classIndex = classString.indexOf(classString);
      classString = classString.substr(0, classIndex) + classString.substr(classIndex + classString.length);
    }

    nameIndex === -1
      ? classString += '' + className
      : classString = classString.substr(0, nameIndex) + classString.substr(nameIndex + className.length);
    element.className = classString;
  }

  printThousandDecimal(number: any, decimals: number = 3): any {
    return parseFloat(parseFloat(number).toFixed(decimals)).toLocaleString();
  }

  isEmptyString(str: string) {
    return this.isUndefined(str) || !str.replace(/\s/g, '').length;
  }

  convertTimezone(d, timezone: number = 8, format: string = 'YYYY-MM-DD HH:mm:ss') {
    if (this.isUndefined(d)) {
      return d;
    }

    d = d.toString();

    if (timezone !== 8) {
      const date = moment.utc(d).subtract(8, 'hours').utcOffset(timezone);
      return date.format(format);
    } else {
      return moment(d).format(format);
    }
  }

  revertTimezone(d, timezone: number = 8, format: string = 'YYYY-MM-DD HH:mm:ss') {
    if (this.isUndefined(d)) {
      return d;
    }

    d = d.toString();

    if (timezone !== 8) {
      let nTimezone = timezone;
      if (timezone >= 0) {
        nTimezone = -Math.abs(timezone);
      } else {
        nTimezone = Math.abs(timezone);
      }

      const date = moment.utc(d).add(8, 'hours').utcOffset(nTimezone);
      return date.format(format);
    } else {
      return moment(d).format(format);
    }
  }

  removeNullObject(obj: object = {}): object {
    for (const propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined) {
            delete obj[propName];
        }
    }

    return obj || {};
  }

  getDefaultRouteAfterLogIn(): object {
    return { name: 'newReceipt' };
  }

  isMobile() {
    return window.innerWidth < SCSS_VARIABLES['sm-min'];
  }

  downloadFile(data, filename: string = 'file', fileExt: string = 'xls') {
    const url = window.URL.createObjectURL(new Blob([data]));
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `${filename}.${fileExt}`);
    document.body.appendChild(link);
    link.click();
  }
}
