import { Vue } from 'vue-property-decorator';

export type VForm = Vue & {
  reset: () => void,
  resetValidation: () => void,
  validate: () => boolean,
};

export type VCropper = Vue & {
  replace: (url, hasSameSize?) => void,
  getCroppedCanvas: () => any,
  reset: () => void,
};

export type VDatePicker = Vue & {
  save: (date) => void,
};
