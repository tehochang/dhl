# WMS Galaxy

## Version
**v1.0.0**

## Dependencies
* nodejs: [https://nodejs.org/](https://nodejs.org/en/)
* vuejs: [https://vuejs.org/](https://vuejs.org/)
* vuecli: [https://cli.vuejs.org/guide/installation.html]

## Getting started

```bash
# Clone the project
git clone git@bitbucket.org:logflows/wms-galaxy.git

# Enter the project directory
cd wms-galaxy

# Install dependency
npm install

# Copy env variables
# Everytime .env.example pls copy it to your local file
cp .env.example .env

# Develop on localhost:9000
npm start
```

## Build

```bash
# Build for development environment
npm run build:dev

# Build for production environment
npm run build:prod
```

## Test

```bash
# Run unit tests (jest)
npm run test:unit

# Run end-to-end tests (cypress)
npm run test:e2e
```

## Advanced

```bash
# Preview the release environment effect on port 9999
npm run build:preview

# Preview the release environment effect + static resource analysis
npm run build:report

# Code format check
npm run lint

# Code format check and auto fix
npm run lint -- --fix
```
