#!/bin/bash

export NODE_ENV=development
export env_config=dev

git reset --hard HEAD
git pull origin development

rm -rf dist/ node_modules/

npm install

npm run build:dev
