'use strict'
const path = require('path');
const webpack = require('webpack');

function resolve(dir) {
  return path.join(__dirname, dir)
}

const port = 9000;

module.exports = {
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  lintOnSave: process.env.NODE_ENV === 'development',
  productionSourceMap: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/styles/mixins/_empty.sass"` // empty file
      },
      scss: {
        prependData: `@import "@/styles/mixins/_mixins.scss";`
      },
    }
  },
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: true,
      errors: true
    }
  },
  configureWebpack: {
    name: process.env.VUE_APP_NAME,
    performance: {
      hints: false
    },
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/
      })
    ],
    resolve: {
      extensions: ['.ts', '.js', '.vue', '.json'],
      alias: {
        '@': resolve('src'),
        // 'moment': 'moment/src/moment'
      }
    }
  },
  chainWebpack: (config) => {
    config.plugins.delete('preload') // TODO: need test
    config.plugins.delete('prefetch') // TODO: need test

    /*config.module
      .rule("raw")
      .test(/\.txt$/)
      .use("raw-loader")
      .loader("raw-loader")
      .end()*/

    // set preserveWhitespace
    /*config.module
      .rule("vue")
      .use("vue-loader")
      .loader("vue-loader")
      .tap(options => {
        options.compilerOptions.preserveWhitespace = true
        return options
      })
      .end()*/

    // https://webpack.js.org/configuration/devtool/#development
    config
      .when(process.env.NODE_ENV === 'development', config => {
          config.devtool('cheap-source-map')
        }
      )

    config
      .when(process.env.NODE_ENV !== "development", config => {
        /*config
          .plugin("TerserPlugin")
          .use("terser-webpack-plugin", [
            {
              parallel: true,
              terserOptions: {
                ecma: 6
              }
            }
          ])
          .end()*/
        config
          .plugin("OptimizeCssAssetsPlugin")
          .use("optimize-css-assets-webpack-plugin", [
            {
              assetNameRegExp: /\.css$/g,
              cssProcessorOptions: {
                safe: true,
                autoprefixer: { disable: true },
                mergeLonghand: false,
                discardComments: {
                  removeAll: true
                }
              },
              canPrint: true
            }
          ])
          .end()
        config
          .plugin("CompressionWebpackPlugin")
          .use("compression-webpack-plugin", [
            {
              filename: "[path].gz[query]",
              algorithm: "gzip",
              test: new RegExp(
                "\\.(" + ["js", "css"].join("|") + ")$"
              ),
              threshold: 10240,
              minRatio: 0.8
            }
          ])
          .end()
        config
          .plugin("ScriptExtHtmlWebpackPlugin")
          .after("html")
          .use("script-ext-html-webpack-plugin", [
            {
              // `runtime` must same as runtimeChunk name. default is `runtime`
              inline: /runtime\..*\.js$/
            }
          ])
          .end()
        config.optimization.splitChunks({
          chunks: "all",
          cacheGroups: {
            libs: {
              name: "chunk-libs",
              test: /[\\/]node_modules[\\/]/,
              priority: 10,
              minSize: 20000,
              maxSize: 400000,
              chunks: "initial" // only package third parties that are initially dependent
            },
            vuetify: {
              name: "chunk-vuetify",
              test: /[\\/]node_modules[\\/]_?vuetify(.*)/,
              priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
              minSize: 20000,
              maxSize: 400000
            },
            commons: {
              name: "chunk-commons",
              test: resolve("src/components"), // can customize your rules
              minChunks: 3, //  minimum common number
              priority: 5,
              minSize: 20000,
              maxSize: 400000,
              reuseExistingChunk: true
            }
          }
        })
        config.optimization.runtimeChunk("single")
      })
  },
  pluginOptions: {},
  transpileDependencies: ['vuetify'],
}
